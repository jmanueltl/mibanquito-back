package com.ninjia.dev.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ninjia.dev.model.Afiliacion;
import com.ninjia.dev.service.IAfiliacionService;

@RestController
@RequestMapping(value = "/afiliaciones")
public class AfiliacionController {

	@Autowired
	private IAfiliacionService service;
	
	@GetMapping
	public List<Afiliacion> listar(){
		return service.listar();
	}
	
	@GetMapping(value = "/{id}")
	public Afiliacion listarPorId(@PathVariable("id") Integer id){
		Optional<Afiliacion> op = service.listarPorId(id);
		return op.isPresent() ? op.get() : new Afiliacion();
	}
	
	
	
	@GetMapping(value = "/afiliados/{idBanco}")
	public List<Afiliacion> listarAfiliadosPorBanco(@PathVariable("idBanco") 
	Integer idBanco){
		return service.listarPorBanco(idBanco);
	}
	

	@GetMapping(value = "/banquitos/{idUsuario}")
	public List<Afiliacion> listarPorUsuario(@PathVariable("idUsuario") 
	Integer idUsuario){
		return service.listarPorSocio(idUsuario);
	}
	
	
	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
	public Afiliacion registrar(@RequestBody Afiliacion afiliacion) {
		return service.registrar(afiliacion);
	}

	@PutMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
	public Afiliacion modificar(@RequestBody Afiliacion afiliacion) {
		return service.modificar(afiliacion);
	}

	@DeleteMapping(value = "/{id}")
	public Integer eliminar(@PathVariable("id") Integer id) {
		Optional<Afiliacion> opt = service.listarPorId(id);
		if (opt.isPresent()) {
			Afiliacion per = new Afiliacion();
			per.setId(id);
			service.eliminar(per);
			return 1;
		}
		return 0;
	}
	
	
}
