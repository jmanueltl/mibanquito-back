package com.ninjia.dev.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ninjia.dev.model.Banco;
import com.ninjia.dev.service.IBancoService;
import com.ninjia.dev.vista.BancoVista;

@RestController
@RequestMapping(value = "/bancos")
public class BancoController {

	@Autowired
	private IBancoService service;
	
	@GetMapping
	public List<Banco> listar(){
		return service.listar();
	}
	
	@GetMapping(value = "/{id}")
	public Banco listarPorId(@PathVariable("id") Integer id){
		Optional<Banco> op = service.listarPorId(id);
		return op.isPresent() ? op.get() : new Banco();
	}
	
	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
	public Banco registrar(@RequestBody Banco persona) {
		return service.registrar(persona);
	}
	
	@PostMapping(value="registrarBanquito",consumes = MediaType.APPLICATION_JSON_VALUE)
	public Banco registrarBanquito(@RequestBody BancoVista bancoVista) {
		return service.registrarBanquito(bancoVista);
	}

	@PutMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
	public Banco modificar(@RequestBody Banco banco) {
		return service.modificar(banco);
	}

	@DeleteMapping(value = "/{id}")
	public Integer eliminar(@PathVariable("id") Integer id) {
		Optional<Banco> opt = service.listarPorId(id);
		if (opt.isPresent()) {
			Banco per = new Banco();
			per.setId(id);
			service.eliminar(per);
			return 1;
		}
		return 0;
	}
	
	
}
