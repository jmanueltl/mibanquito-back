package com.ninjia.dev.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ninjia.dev.model.Invitacion;
import com.ninjia.dev.service.IInvitacionService;
import com.ninjia.dev.vista.InvitacionVista;;

@RestController
@RequestMapping(value = "/invitaciones")
public class InvitacionController {

	@Autowired
	private IInvitacionService service;
	
	@GetMapping
	public List<Invitacion> listar(){
		return service.listar();
	}
	
	@GetMapping(value = "/{id}")
	public Invitacion listarPorId(@PathVariable("id") Integer id){
		Optional<Invitacion> op = service.listarPorId(id);
		return op.isPresent() ? op.get() : new Invitacion();
	}
	

	@GetMapping(value = "/DNI/{dni}")
	public List<Invitacion> listarPorDni(@PathVariable("dni") String dni){
		List<Invitacion> lista = service.listarPorDni(dni);
		return lista;
	}
	
	@GetMapping(value = "/celular/{celular}")
	public List<Invitacion> listarPorCelular(@PathVariable("celular") String celular){
		List<Invitacion> lista = service.listarPorCelular(celular);
		return lista;
	}
	
	
	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
	public Invitacion registrar(@RequestBody Invitacion persona) {
		return service.registrar(persona);
	}
	
	@PostMapping(value="gestionarInvitacion",consumes = MediaType.APPLICATION_JSON_VALUE)
	public Invitacion gestionarInvitacion(@RequestBody InvitacionVista invitacionVista) {
		return service.gestionarInvitacion(invitacionVista);
	}

	@PutMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
	public Invitacion modificar(@RequestBody Invitacion banco) {
		return service.modificar(banco);
	}

	
	@DeleteMapping(value = "/{id}")
	public Integer eliminar(@PathVariable("id") Integer id) {
		Optional<Invitacion> opt = service.listarPorId(id);
		if (opt.isPresent()) {
			Invitacion per = new Invitacion();
			per.setId(id);
			service.eliminar(per);
			return 1;
		}
		return 0;
	}
	
	
}
