package com.ninjia.dev.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ninjia.dev.model.Movimiento;
import com.ninjia.dev.service.IMovimientoService;
import com.ninjia.dev.vista.MovimientoVista;

@RestController
@RequestMapping(value = "/movimientos")
public class MovimientoController {

	@Autowired
	private IMovimientoService service;
	
	@GetMapping
	public List<Movimiento> listar(){
		return service.listar();
	}
	
	@GetMapping(value = "/Banco/{idBanco}")
	public List<MovimientoVista> listarPorBanco(@PathVariable("idBanco") Integer idBanco){
		return service.listarPorBanco(idBanco);
	}
	
}
