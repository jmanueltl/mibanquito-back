package com.ninjia.dev.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ninjia.dev.model.Prestamo;
import com.ninjia.dev.service.IPrestamoService;
import com.ninjia.dev.vista.AprobarVista;
import com.ninjia.dev.vista.PrestamoVista;
import com.ninjia.dev.vista.Respuesta;

@RestController
@RequestMapping(value = "/prestamos")
public class PrestamoController {

	@Autowired
	private IPrestamoService service;
	
	@GetMapping
	public List<Prestamo> listar(){
		return service.listar();
	}
	
	@GetMapping(value = "/{id}")
	public Prestamo listarPorId(@PathVariable("id") Integer id){
		Optional<Prestamo> op = service.listarPorId(id);
		return op.isPresent() ? op.get() : new Prestamo();
	}
	
	
	@GetMapping(value = "listarPorBanco/{idBanco}")
	public List<Prestamo> listarPorBanco(@PathVariable("idBanco") Integer idBanco){		
		return service.listarPorBanco(idBanco);
	}
	
	
	
	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
	public Prestamo registrar(@RequestBody PrestamoVista prestamoVista) {
		return service.registrar(prestamoVista);
	}
	
	@PostMapping(value="aprobar" ,consumes = MediaType.APPLICATION_JSON_VALUE)
	public Respuesta aprobar(@RequestBody AprobarVista aprobarVista) {
		return service.aprobar(aprobarVista);
	}
	

	@PutMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
	public Prestamo modificar(@RequestBody Prestamo prestamo) {
		return service.modificar(prestamo);
	}

	@DeleteMapping(value = "/{id}")
	public Integer eliminar(@PathVariable("id") Integer id) {
		Optional<Prestamo> opt = service.listarPorId(id);
		if (opt.isPresent()) {
			Prestamo per = new Prestamo();
			per.setId(id);
			service.eliminar(per);
			return 1;
		}
		return 0;
	}
	
	
}
