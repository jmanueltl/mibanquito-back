package com.ninjia.dev.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ninjia.dev.model.Afiliacion;
import com.ninjia.dev.model.Banco;
import com.ninjia.dev.model.Usuario;

public interface IAfiliacionDAO extends JpaRepository<Afiliacion, Integer>{
	public List<Afiliacion> findByUsuarioSocio(Usuario UsuarioSocio);
	public List<Afiliacion> findByBanco(Banco banco);
}
