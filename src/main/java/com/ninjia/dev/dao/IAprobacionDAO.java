package com.ninjia.dev.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.ninjia.dev.model.Aprobacion;

public interface IAprobacionDAO extends JpaRepository<Aprobacion, Integer>{
	
	@Query("select a from Aprobacion a where a.prestamo.id = :idPrestamo and a.usuarioCalificador.id = :idUsuarioCalificador")
	public Aprobacion findAprobacionByPrestamoCalificador(Integer idPrestamo,Integer idUsuarioCalificador);
}
