package com.ninjia.dev.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ninjia.dev.model.Banco;

public interface IBancoDAO extends JpaRepository<Banco, Integer>{
	

}
