package com.ninjia.dev.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ninjia.dev.model.Invitacion;

public interface IInvitacionDAO extends JpaRepository<Invitacion, Integer>{
	public List<Invitacion> findByDni(String dni);
	public List<Invitacion> findByNumeroCelular(String numeroCelular);
}
