package com.ninjia.dev.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ninjia.dev.model.Banco;
import com.ninjia.dev.model.Movimiento;

public interface IMovimientoDAO extends JpaRepository<Movimiento, Integer>{
	public List<Movimiento> findByBanco(Banco banco);
}
