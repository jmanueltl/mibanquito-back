package com.ninjia.dev.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.ninjia.dev.model.Afiliacion;
import com.ninjia.dev.model.Prestamo;

public interface IPrestamoDAO extends JpaRepository<Prestamo, Integer>{
	public List<Prestamo> findByAfiliacion(Afiliacion afiliacion);

	@Query("select p from Prestamo p where p.afiliacion.id in(select a.id from Afiliacion a where a.banco.id=:idBanco)")
	public List<Prestamo> listarPorBanco(Integer idBanco);
}
