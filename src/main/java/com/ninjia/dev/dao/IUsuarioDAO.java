package com.ninjia.dev.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ninjia.dev.model.Usuario;

public interface IUsuarioDAO extends JpaRepository<Usuario, Integer>{
	public Usuario findByNumeroCelular(String numeroCelular);
	Usuario getByNumeroCelularAndContrasenia(String celular,String contrasenia);
}
