package com.ninjia.dev.model;

import java.io.Serializable;

public class CompositeKey implements Serializable{

	/**
	 * 
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Integer prestamo;
	private Integer usuarioCalificador;
	
	public CompositeKey() {}//NOSONAR

	public Integer getPrestamo() {
		return prestamo;
	}

	public void setPrestamo(Integer prestamo) {
		this.prestamo = prestamo;
	}

	public Integer getUsuarioCalificador() {
		return usuarioCalificador;
	}

	public void setUsuarioCalificador(Integer usuarioCalificador) {
		this.usuarioCalificador = usuarioCalificador;
	}	
	

}
