package com.ninjia.dev.service;

import java.util.List;
import java.util.Optional;

import com.ninjia.dev.model.Afiliacion;

public interface IAfiliacionService {
	
	List<Afiliacion> listar();
	Optional<Afiliacion> listarPorId(Integer id);	
	Afiliacion registrar(Afiliacion per);
	Afiliacion modificar(Afiliacion per);
	void eliminar(Afiliacion per);
	List<Afiliacion> listarPorSocio(Integer idUsuario);
	List<Afiliacion> listarPorBanco(Integer idBanco);

}
