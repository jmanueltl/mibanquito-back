package com.ninjia.dev.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ninjia.dev.dao.IAfiliacionDAO;
import com.ninjia.dev.model.Afiliacion;
import com.ninjia.dev.model.Banco;
import com.ninjia.dev.model.Usuario;

@Service
public class IAfiliacionServiceImpl implements IAfiliacionService {

	@Autowired
	private IAfiliacionDAO dao;

	@Override
	public List<Afiliacion> listar() {
		return dao.findAll();
	}

	@Override
	public Optional<Afiliacion> listarPorId(Integer id) {		
		return dao.findById(id);
	}

	@Override
	public Afiliacion registrar(Afiliacion per) {
		return dao.save(per);
	}

	@Override
	public Afiliacion modificar(Afiliacion per) {
		Optional<Afiliacion> persona = dao.findById(per.getId());
		
		if (persona.isPresent()) {
			return dao.save(per);
		}
		return new Afiliacion();
	}

	@Override
	public void eliminar(Afiliacion per) {
		dao.delete(per);
	}

	@Override
	public List<Afiliacion> listarPorSocio(Integer idUsuario) {
		Usuario usuarioSocio=new Usuario();
		usuarioSocio.setId(idUsuario);
		return dao.findByUsuarioSocio(usuarioSocio);
	}

	@Override
	public List<Afiliacion> listarPorBanco(Integer idBanco) {
		Banco banco=new Banco();
		banco.setId(idBanco);		
		return dao.findByBanco(banco);
	}
}
