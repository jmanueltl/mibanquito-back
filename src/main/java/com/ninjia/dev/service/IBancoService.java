package com.ninjia.dev.service;

import java.util.List;
import java.util.Optional;

import com.ninjia.dev.model.Banco;
import com.ninjia.dev.vista.BancoVista;

public interface IBancoService {
	
	List<Banco> listar();
	Optional<Banco> listarPorId(Integer id);	
	Banco registrar(Banco per);
	Banco modificar(Banco per);
	void eliminar(Banco per);
	Banco registrarBanquito(BancoVista bancoVista);
}
