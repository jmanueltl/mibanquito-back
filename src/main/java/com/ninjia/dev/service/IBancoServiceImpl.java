package com.ninjia.dev.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ninjia.dev.dao.IAfiliacionDAO;
import com.ninjia.dev.dao.IBancoDAO;
import com.ninjia.dev.dao.IInvitacionDAO;
import com.ninjia.dev.dao.IUsuarioDAO;
import com.ninjia.dev.model.Afiliacion;
import com.ninjia.dev.model.Banco;
import com.ninjia.dev.model.Invitacion;
import com.ninjia.dev.util.Constante;
import com.ninjia.dev.util.Util;
import com.ninjia.dev.vista.BancoVista;

@Service
public class IBancoServiceImpl implements IBancoService {

	@Autowired
	private IBancoDAO bancoDAO;
	
	@Autowired
	private IInvitacionDAO iInvitacionDAO;
	@Autowired
	private IUsuarioDAO iUsuarioDAO;
	
	@Autowired
	private IAfiliacionDAO afiliacionDAO;

    @Autowired
    public IEmailService emailService;
	@Override
	public List<Banco> listar() {
		return bancoDAO.findAll();
	}

	@Override
	public Optional<Banco> listarPorId(Integer id) {		
		return bancoDAO.findById(id);
	}

	@Override
	public Banco registrar(Banco per) {
		return bancoDAO.save(per);
	}

	@Override
	public Banco modificar(Banco per) {
		Optional<Banco> persona = bancoDAO.findById(per.getId());
		
		if (persona.isPresent()) {
			return bancoDAO.save(per);
		}
		return new Banco();
	}

	@Override
	public void eliminar(Banco per) {
		bancoDAO.delete(per);
	}

	@Override
	public Banco registrarBanquito(BancoVista bancoVista) {//NOSONAR
		Banco banco=null;
		try {			
			if(bancoVista!=null) {
				banco=bancoVista.getBanco();
				banco.setFechaCreacion(new Date());
				banco=bancoDAO.save(banco);
				Afiliacion afiliacion=new Afiliacion();
				afiliacion.setBanco(banco);
				afiliacion.setUsuarioSocio(banco.getUsuarioCreador());
				afiliacion.setFechaAfiliacion(new Date());
				afiliacion.setTipo("A");
				afiliacionDAO.save(afiliacion);
				List<Invitacion> listInvitacion=bancoVista.getListaInvitados();
				StringBuilder numCells=new StringBuilder();
				List<String> recipients=new ArrayList<String>();
				if(listInvitacion!=null && !listInvitacion.isEmpty()) {
					for(Invitacion inv:listInvitacion) {
						inv.setEstado(Constante.INVITACION_ENVIADO);
						inv.setFechaEnvio(new Date());
						inv.setBanco(banco);
						numCells.append(Constante.PREFIJO_PERU+inv.getNumeroCelular()).append(",");						
						iInvitacionDAO.save(inv);
						recipients.add(inv.getCorreoElectronico());						
					}
					if(bancoVista.getNotificar()) {
						Util.sendSms("EL BANQUITO " + banco.getNombre() +" le envio una invitacion,palabra clave:"+banco.getPalabraClave(), numCells.toString().substring(0,numCells.toString().length()-1));
					}
				}
			
			}
			
		} catch (Exception e) {
		}
		return banco;
	}

}
