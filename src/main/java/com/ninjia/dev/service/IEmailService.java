package com.ninjia.dev.service;

import java.util.List;

import org.springframework.mail.SimpleMailMessage;
public interface IEmailService {
    void sendSimpleMessage(String to,
            String subject,
            String text);
void sendSimpleMessageUsingTemplate(String to,
                         String subject,
                         SimpleMailMessage template,
                         String ...templateArgs);

public void sendMailAsBcc(List<String> recipients, String subject, String text);

}
