package com.ninjia.dev.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

/**
 * 
 */
@Component
public class IEmailServiceImpl implements IEmailService {

    @Autowired
    public JavaMailSender emailSender;
    @Async
    public void sendSimpleMessage(String to, String subject, String text) {
        try {
            SimpleMailMessage message = new SimpleMailMessage();
            message.setTo(to);
            message.setSubject(subject);
            message.setText(text);

            emailSender.send(message);
        } catch (MailException exception) {
            exception.printStackTrace();
        }
    }

    @Async
    public void sendMailAsBcc(List<String> recipients, String subject, String text) {//NOSONAR
        if (recipients.isEmpty()) {
            throw new IllegalArgumentException("recipients is empty.");
        }
        SimpleMailMessage message = new SimpleMailMessage();
        message.setBcc(recipients.toArray(new String[recipients.size()]));
        message.setSubject(subject);
        message.setText(text);
        try {
        	emailSender.send(message);
        } catch (MailException e) {
        }
    }
    
    
    
    @Override
    public void sendSimpleMessageUsingTemplate(String to,
                                               String subject,
                                               SimpleMailMessage template,
                                               String ...templateArgs) {
        String text = String.format(template.getText(), templateArgs);  
        sendSimpleMessage(to, subject, text);
    }


  
}
