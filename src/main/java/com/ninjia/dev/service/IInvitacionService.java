package com.ninjia.dev.service;

import java.util.List;
import java.util.Optional;

import com.ninjia.dev.model.Invitacion;
import com.ninjia.dev.vista.InvitacionVista;;

public interface IInvitacionService {
	
	List<Invitacion> listar();
	Optional<Invitacion> listarPorId(Integer id);	
	Invitacion registrar(Invitacion per);
	Invitacion modificar(Invitacion per);
	void eliminar(Invitacion per);
	List<Invitacion> listarPorDni(String dni);
    Invitacion gestionarInvitacion(InvitacionVista invitacionVista);
    List<Invitacion> listarPorCelular(String celular);
}
