package com.ninjia.dev.service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ninjia.dev.dao.IAfiliacionDAO;
import com.ninjia.dev.dao.IBancoDAO;
import com.ninjia.dev.dao.IInvitacionDAO;
import com.ninjia.dev.model.Afiliacion;
import com.ninjia.dev.model.Banco;
import com.ninjia.dev.model.Invitacion;
import com.ninjia.dev.model.Usuario;
import com.ninjia.dev.vista.InvitacionVista;

@Service
public class IInvitacionServiceImpl implements IInvitacionService {

	@Autowired
	private IInvitacionDAO dao;	
	@Autowired
	private IAfiliacionDAO afiliacionDAO;
	@Autowired
	private IBancoDAO bancoDAO;
	
	@Override
	public List<Invitacion> listar() {
		return dao.findAll();
	}

	@Override
	public Optional<Invitacion> listarPorId(Integer id) {		
		return dao.findById(id);
	}

	@Override
	public Invitacion registrar(Invitacion per) {
		return dao.save(per);
	}

	@Override
	public Invitacion modificar(Invitacion per) {
		Optional<Invitacion> persona = dao.findById(per.getId());
		
		if (persona.isPresent()) {
			return dao.save(per);
		}
		return new Invitacion();
	}

	@Override
	public void eliminar(Invitacion per) {
		dao.delete(per);
	}

	@Override
	public List<Invitacion> listarPorDni(String dni) {
		return dao.findByDni(dni);
	}

	@Override
	public Invitacion gestionarInvitacion(InvitacionVista invitacionVista) {
		Invitacion invitacion=null;
		try {	
			if(invitacionVista!=null) {
				
				Banco banco=bancoDAO.findById(invitacionVista.getIdBanco()).orElse(null);
				invitacion=dao.findById(invitacionVista.getId()).orElse(null);
				if(banco!=null && invitacion!=null) {
					if(invitacionVista.getEstado().equals("A")
							&& invitacionVista.getPalabraClave().equals(banco.getPalabraClave())) {
						invitacion.setEstado(invitacionVista.getEstado());
						dao.save(invitacion);						
						Afiliacion afiliacion=new Afiliacion();
						afiliacion.setEstado(true);
						afiliacion.setFechaAfiliacion(new Date());
						afiliacion.setTipo("S");
						afiliacion.setFechaAfiliacion(new Date());
						afiliacion.setBanco(banco);
						Usuario usuario=new Usuario();
						usuario.setId(invitacionVista.getIdUsuario());
						afiliacion.setUsuarioSocio(usuario);
						afiliacionDAO.save(afiliacion);
					}
					if(invitacionVista.getEstado().equals("R")) {						
						invitacion.setEstado(invitacionVista.getEstado());
						dao.save(invitacion);
					}
				}
				

			}
		} catch (Exception e) {
		}
		return invitacion;
	}

	@Override
	public List<Invitacion> listarPorCelular(String numeroCelular) {
		return dao.findByNumeroCelular(numeroCelular);
	}
}
