package com.ninjia.dev.service;

import java.util.List;
import java.util.Optional;

import com.ninjia.dev.model.Movimiento;
import com.ninjia.dev.vista.MovimientoVista;

public interface IMovimientoService {
	
	List<Movimiento> listar();
	Optional<Movimiento> listarPorId(Integer id);	
	Movimiento registrar(Movimiento per);
	Movimiento modificar(Movimiento per);
	void eliminar(Movimiento per);
	List<MovimientoVista> listarPorBanco(Integer idBanco);

}
