package com.ninjia.dev.service;

import java.math.BigDecimal;
import java.math.MathContext;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ninjia.dev.dao.IMovimientoDAO;
import com.ninjia.dev.dao.IUsuarioDAO;
import com.ninjia.dev.model.Banco;
import com.ninjia.dev.model.Movimiento;
import com.ninjia.dev.model.Usuario;
import com.ninjia.dev.vista.MovimientoVista;
@Service
public class IMovimientoServiceImpl implements IMovimientoService {

	@Autowired
	private IMovimientoDAO dao;
	
	@Autowired
	private IUsuarioDAO usuarioDao;

	@Override
	public List<Movimiento> listar() {
		return dao.findAll();
	}

	@Override
	public Optional<Movimiento> listarPorId(Integer id) {		
		return dao.findById(id);
	}

	@Override
	public List<MovimientoVista> listarPorBanco(Integer idBanco) {
		Banco banco=new Banco();
		banco.setId(idBanco);
		List<MovimientoVista> movimientosVista=new ArrayList<MovimientoVista>();
		List<Movimiento> movimientos=dao.findByBanco(banco);
		BigDecimal mtoAcumulado=new BigDecimal(0);
		MathContext mc = new MathContext(2); // 2 precision
		
		for(int i=0;i<movimientos.size();i++) {
			if(movimientos.get(i).getTipoMovimiento().equals("DEPOSITO")) {
				mtoAcumulado=mtoAcumulado.add(movimientos.get(i).getMontoMovimiento(),mc);
			}else {
				mtoAcumulado=mtoAcumulado.subtract(movimientos.get(i).getMontoMovimiento(),mc);
			}
		}
		
		for(int i=0;i<movimientos.size();i++) {
			MovimientoVista movimientoVista=new MovimientoVista();
			movimientoVista.setMovimiento(movimientos.get(i));
			Usuario usuario=usuarioDao.findByNumeroCelular(movimientos.get(i).getNumCelular());
			movimientoVista.setUsuario(usuario);
			movimientoVista.setMtoAcumulado(mtoAcumulado);
			movimientosVista.add(movimientoVista);
		}
		
		return movimientosVista;
	}

	@Override
	public Movimiento registrar(Movimiento per) {
		return null;
	}

	@Override
	public Movimiento modificar(Movimiento per) {
		return null;
	}

	@Override
	public void eliminar(Movimiento per) {
		//Comentario
	}

}
