package com.ninjia.dev.service;

import java.util.List;
import java.util.Optional;

import com.ninjia.dev.model.Afiliacion;
import com.ninjia.dev.model.Prestamo;
import com.ninjia.dev.vista.AprobarVista;
import com.ninjia.dev.vista.PrestamoVista;
import com.ninjia.dev.vista.Respuesta;

public interface IPrestamoService {
	
	List<Prestamo> listar();
	Optional<Prestamo> listarPorId(Integer id);	
	Prestamo registrar(PrestamoVista prestamoVista);
	Prestamo modificar(Prestamo per);
	void eliminar(Prestamo per);
	List<Prestamo> listarPorAfiliacion(Afiliacion afiliacion);
	List<Prestamo> listarPorBanco(Integer idBanco);
	Respuesta aprobar(AprobarVista aprobarVista);

}
