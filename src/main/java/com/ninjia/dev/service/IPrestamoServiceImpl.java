package com.ninjia.dev.service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ninjia.dev.dao.IAfiliacionDAO;
import com.ninjia.dev.dao.IAprobacionDAO;
import com.ninjia.dev.dao.IMovimientoDAO;
import com.ninjia.dev.dao.IPrestamoDAO;
import com.ninjia.dev.model.Afiliacion;
import com.ninjia.dev.model.Aprobacion;
import com.ninjia.dev.model.Movimiento;
import com.ninjia.dev.model.Prestamo;
import com.ninjia.dev.model.Usuario;
import com.ninjia.dev.util.Constante;
import com.ninjia.dev.util.Util;
import com.ninjia.dev.vista.AprobarVista;
import com.ninjia.dev.vista.PrestamoVista;
import com.ninjia.dev.vista.Respuesta;

@Service
public class IPrestamoServiceImpl implements IPrestamoService {

	@Autowired
	private IPrestamoDAO dao;
	@Autowired
	private IAprobacionDAO iAprobacionDAO;
	
	@Autowired
	private IAfiliacionDAO iAfiliacionDAO;
	@Autowired
	private IMovimientoDAO iMovimientoDAO;


	@Override
	public List<Prestamo> listar() {
		return dao.findAll();
	}

	@Override
	public Optional<Prestamo> listarPorId(Integer id) {		
		return dao.findById(id);
	}

	@Override
	public Prestamo registrar(PrestamoVista prestamoVista) {
		try {
			Prestamo prestamo=new Prestamo();
			prestamo.setEstado(Constante.PRESTAMO_SOLICITADO);
			prestamo.setFechaSolicitud(new Date());
			prestamo.setMontoSolicitado(prestamoVista.getMontoSolicitado());
			prestamo.setMontoApagar(prestamoVista.getMontoPagar());
		
			Date dfechaPago = Util.stringToDate(prestamoVista.getFechaPago());
			prestamo.setFechaMaximoPago(dfechaPago);
			Afiliacion afiliacion=new Afiliacion();
			afiliacion.setId(prestamoVista.getIdAfiliacion());		
			prestamo.setAfiliacion(afiliacion);		
			return dao.save(prestamo);
		} catch (Exception e) {
			return null;
		}

	}

	@Override
	public Prestamo modificar(Prestamo per) {
		Optional<Prestamo> persona = dao.findById(per.getId());
		
		if (persona.isPresent()) {
			return dao.save(per);
		}
		return new Prestamo();
	}

	@Override
	public void eliminar(Prestamo per) {
		dao.delete(per);
	}

	@Override
	public List<Prestamo> listarPorAfiliacion(Afiliacion afiliacion) {
		return dao.findByAfiliacion(afiliacion);
	}

	@Override
	public List<Prestamo> listarPorBanco(Integer idBanco) {
		return dao.listarPorBanco(idBanco);
	}

	@Override
	public Respuesta aprobar(AprobarVista aprobarVista) {//NOSONAR
		Respuesta respuesta=new Respuesta();		
		try {
			
			Optional<Prestamo> oPrestamo=dao.findById(aprobarVista.getIdPrestamo());
			Prestamo prestamo=oPrestamo.isPresent() ? oPrestamo.get() : null;
			if(prestamo!=null) {
				Aprobacion aprobacion=new Aprobacion();
				aprobacion.setAprobado(aprobarVista.getAprobado());
				aprobacion.setFechaCalifiacion(new Date());
				aprobacion.setPrestamo(prestamo);
				Usuario usuario=new Usuario();
				usuario.setId(aprobarVista.getIdUsuarioCalificador());
				aprobacion.setUsuarioCalificador(usuario);
				aprobacion=iAprobacionDAO.save(aprobacion);
				respuesta.setEntidad(aprobacion);
				List<Afiliacion> listaAfiliaciones=iAfiliacionDAO.findByBanco(
						prestamo.getAfiliacion().getBanco());
				boolean retirar=true;
				if(listaAfiliaciones!=null && !listaAfiliaciones.isEmpty()) {
					for(Afiliacion afiliaciones:listaAfiliaciones) {
						if(!afiliaciones.getUsuarioSocio().getId().
								equals(aprobacion.getUsuarioCalificador().getId())) {
							Aprobacion aproba=iAprobacionDAO.findAprobacionByPrestamoCalificador(aprobacion.getPrestamo().getId(),
									aprobacion.getUsuarioCalificador().getId());
							
							if(aproba==null || !aproba.getAprobado()) {
								retirar=false;
								prestamo.setEstado(Constante.PRESTAMO_RECHAZADO);
								break;
							}
							
						}
					}
					
					if(retirar) {
						//cambiar estado Prestamo
						prestamo.setEstado(Constante.PRESTAMO_ACEPTADO);				
						//ingresa movimiento
						Movimiento movimiento=new Movimiento();
						movimiento.setBanco(prestamo.getAfiliacion().getBanco());
						movimiento.setFechaMovimiento(new Date());
						movimiento.setMontoMovimiento(prestamo.getMontoSolicitado());
						movimiento.setNumCelular(prestamo.getAfiliacion().getUsuarioSocio().getNumeroCelular());
						movimiento.setNumCuenta(prestamo.getAfiliacion().getBanco().getNumCuenta());
						movimiento.setTipoMovimiento(Constante.MOVIMIENTO_RETIRO);
						iMovimientoDAO.save(movimiento);
						//enviar SMS COBRO
						Util.sendSms("Hola "+prestamo.getAfiliacion().getUsuarioSocio().getNombres()+" ya puede ir a cobrar", Constante.PREFIJO_PERU+movimiento.getNumCelular());
						
						
					}
					dao.save(prestamo);
				}
			}
	
			
			
			
			respuesta.setMensaje("Procesado Correctamente");
			respuesta.setResultado(true);
		} catch (Exception e) {
			respuesta.setMensaje("Error al procesar Aprobacion");
			respuesta.setResultado(false);
		}
		return respuesta;
	}
}
