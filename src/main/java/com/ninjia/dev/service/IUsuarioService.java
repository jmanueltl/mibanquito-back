package com.ninjia.dev.service;

import java.util.List;
import java.util.Optional;

import com.ninjia.dev.model.Usuario;

public interface IUsuarioService {
	
	List<Usuario> listar();
	Optional<Usuario> listarPorId(Integer id);	
	Usuario registrar(Usuario usuario);
	Usuario modificar(Usuario usuario);
	void eliminar(Usuario usuario);
	Usuario login(String celular,String contrasenia);

}
