package com.ninjia.dev.service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ninjia.dev.dao.IUsuarioDAO;
import com.ninjia.dev.model.Usuario;
import com.ninjia.dev.util.Util;

@Service
public class UsuarioServiceImpl implements IUsuarioService {

	@Autowired
	private IUsuarioDAO dao;

	@Override
	public List<Usuario> listar() {
		return dao.findAll();
	}

	@Override
	public Optional<Usuario> listarPorId(Integer id) {		
		return dao.findById(id);
	}

	@Override
	public Usuario registrar(Usuario per) {
		if(per.getStrfechaNacimiento()!=null) {
			Date fechaNacimiento =Util.stringToDate(per.getStrfechaNacimiento());
			per.setFechaNacimiento(fechaNacimiento);
		}
		return dao.save(per);
	}

	@Override
	public Usuario modificar(Usuario per) {
		Optional<Usuario> persona = dao.findById(per.getId());
		
		if (persona.isPresent()) {
			if(per.getStrfechaNacimiento()!=null) {
				Date fechaNacimiento =Util.stringToDate(per.getStrfechaNacimiento());
				per.setFechaNacimiento(fechaNacimiento);
			}			
			return dao.save(per);
		}
		return new Usuario();
	}

	@Override
	public void eliminar(Usuario per) {
		dao.delete(per);

	}

	@Override
	public Usuario login(String celular,String contrasenia) {		
		return dao.getByNumeroCelularAndContrasenia(celular,contrasenia);
	}

}
