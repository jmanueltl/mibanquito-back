package com.ninjia.dev.util;

public class Constante {
	private Constante() {}
	public static final String INVITACION_ENVIADO="0";
	public static final String INVITACION_ACEPTADO="1";
	public static final String INVITACION_RECHAZADO="2";
	//PRESTAMO
	public static final String PRESTAMO_SOLICITADO="S";
	public static final String PRESTAMO_RECHAZADO="R";
	public static final String PRESTAMO_ACEPTADO="A";
	//MOVIMIENTO
	public static final String MOVIMIENTO_RETIRO="RETIRO";
	public static final String MOVIMIENTO_APORTE="ABONO";
	
	public static final String PREFIJO_PERU="51";
	
}
