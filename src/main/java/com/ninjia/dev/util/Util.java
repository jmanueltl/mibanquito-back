package com.ninjia.dev.util;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Util {
	private Util() {}
	public static Date stringToDate(String dateAsString) {
		
		DateFormat sourceFormat = new SimpleDateFormat("dd/MM/yyyy");
		Date dfechaPago = null;
		try {
			dfechaPago = sourceFormat.parse(dateAsString);
		} catch (ParseException e) {
		}
		return dfechaPago;
	}
	public static String sendSms(String cuerpoSMS,String listaNumbers) {
		try {
			// Construct data
			String message = "&message=" + cuerpoSMS;
			String sender = "&sender=" + "BANQUITO BBVA";
			String numbers = "&numbers=" + listaNumbers;
			
			String user = "username=" + "alfredfis@gmail.com";
			String hash = "&hash=" + "ef1cc2da3e81851c5100cd32daa26ef6c6dc0ec3ee03f3aad7289d3143784fcc";
			
			// Send data
			HttpURLConnection conn = (HttpURLConnection) new URL("https://api.txtlocal.com/send/?").openConnection();
			String data = user + hash+numbers + message + sender;
			conn.setDoOutput(true);
			conn.setRequestMethod("POST");
			conn.setRequestProperty("Content-Length", Integer.toString(data.length()));
			conn.getOutputStream().write(data.getBytes("UTF-8"));
			final BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
			final StringBuilder stringBuffer = new StringBuilder();
			String line;
			while ((line = rd.readLine()) != null) {
				stringBuffer.append(line);
			}
			rd.close();
			
			return stringBuffer.toString();
		} catch (Exception e) {
			return "Error "+e;
		}
	}
}
