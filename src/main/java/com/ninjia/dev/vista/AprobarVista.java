package com.ninjia.dev.vista;

public class AprobarVista {
	private Boolean aprobado;
	private Integer idUsuarioCalificador;
	private Integer idPrestamo;
	public Boolean getAprobado() {
		return aprobado;
	}
	public void setAprobado(Boolean aprobado) {
		this.aprobado = aprobado;
	}
	public Integer getIdUsuarioCalificador() {
		return idUsuarioCalificador;
	}
	public void setIdUsuarioCalificador(Integer idUsuarioCalificador) {
		this.idUsuarioCalificador = idUsuarioCalificador;
	}
	public Integer getIdPrestamo() {
		return idPrestamo;
	}
	public void setIdPrestamo(Integer idPrestamo) {
		this.idPrestamo = idPrestamo;
	}	
}
