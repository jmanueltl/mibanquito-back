package com.ninjia.dev.vista;

import java.util.List;

import com.ninjia.dev.model.Banco;
import com.ninjia.dev.model.Invitacion;

public class BancoVista {
	private Banco banco;
	private List<Invitacion> listaInvitados;
	private Boolean notificar;
	public Banco getBanco() {
		return banco;
	}
	public void setBanco(Banco banco) {
		this.banco = banco;
	}
	public List<Invitacion> getListaInvitados() {
		return listaInvitados;
	}
	public void setListaInvitados(List<Invitacion> listaInvitados) {
		this.listaInvitados = listaInvitados;
	}
	public Boolean getNotificar() {
		return notificar;
	}
	public void setNotificar(Boolean notificar) {
		this.notificar = notificar;
	}
	
	

}
