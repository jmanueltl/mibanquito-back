package com.ninjia.dev.vista;

import java.math.BigDecimal;

import com.ninjia.dev.model.Movimiento;
import com.ninjia.dev.model.Usuario;

public class MovimientoVista {
	private Movimiento movimiento;
	public Movimiento getMovimiento() {
		return movimiento;
	}
	public void setMovimiento(Movimiento movimiento) {
		this.movimiento = movimiento;
	}
	
	private Usuario usuario;
    public Usuario getUsuario() {
		return usuario;
	}
	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	
	private BigDecimal mtoAcumulado;
	public BigDecimal getMtoAcumulado() {
		return mtoAcumulado;
	}
	public void setMtoAcumulado(BigDecimal mtoAcumulado) {
		this.mtoAcumulado = mtoAcumulado;
	}
}
