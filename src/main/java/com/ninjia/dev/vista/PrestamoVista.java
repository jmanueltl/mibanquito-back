package com.ninjia.dev.vista;

import java.math.BigDecimal;

public class PrestamoVista {
	private BigDecimal montoSolicitado;
	private String fechaPago;
	private BigDecimal montoPagar;
	private Integer idAfiliacion;
	public BigDecimal getMontoSolicitado() {
		return montoSolicitado;
	}
	public void setMontoSolicitado(BigDecimal montoSolicitado) {
		this.montoSolicitado = montoSolicitado;
	}
	public String getFechaPago() {
		return fechaPago;
	}
	public void setFechaPago(String fechaPago) {
		this.fechaPago = fechaPago;
	}
	public BigDecimal getMontoPagar() {
		return montoPagar;
	}
	public void setMontoPagar(BigDecimal montoPagar) {
		this.montoPagar = montoPagar;
	}

	public Integer getIdAfiliacion() {
		return idAfiliacion;
	}
	public void setIdAfiliacion(Integer idAfiliacion) {
		this.idAfiliacion = idAfiliacion;
	}
	
	
}
