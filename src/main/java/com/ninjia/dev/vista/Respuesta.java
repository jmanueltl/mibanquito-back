package com.ninjia.dev.vista;

public class Respuesta {
	private Object entidad;
	private String mensaje;
	private boolean resultado;
	public Object getEntidad() {
		return entidad;
	}
	public void setEntidad(Object entidad) {
		this.entidad = entidad;
	}
	public String getMensaje() {
		return mensaje;
	}
	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}
	public boolean isResultado() {
		return resultado;
	}
	public void setResultado(boolean resultado) {
		this.resultado = resultado;
	}
	
}
